# KKu Island <small>(꾸왈!)</small>
Ubuntu 16.04 
> DockerHub 에서 ubuntu16.04 버전을 받아온다.\
docker pull  tagname:tag  허브에서 tagname 과 tag 로 Docker Image를 찾아 받는다. \
<a href="https://hub.docker.com/_/ubuntu?tab=tags"> Docker Hub </a>에서 버전들을 확인 할 수 있다.
```
$ docker pull ubuntu:16.04
```
>허브에서 받아온 Docker Image를 확인 할 수 있다.  
```
$ docker images
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
ubuntu              16.04               5f2bf26e3524        3 weeks ago         123MB
```
> 이미지를 컨테이너로 올려 실행 시켜야한다.\
컨테이너로 올릴때 수많은 옵션들이 존재한다. 여기선 딱 업무에 필요한 내용만 설정해 보겠습니다. \
설명이 잘되어있는 곳을 <a href="http://pyrasis.com/book/DockerForTheReallyImpatient/Chapter20/28">한번 읽어보면 도움이 된다.</a>\
-i 표준입력 유지\
-t 명령을 입력할 수는 있지만 셸이 표시되지 않습니다 \
-p 호스트에 연결된 컨테이너의 특정 포트를 외부에 노출  \
-v 호스트와 공유할 디렉터리를 설정하여 파일을 컨테이너에 저장하지 않고 호스트에 바로 저장
* 경로설정을 위해 windows_path 설정을 먼저 적용하고 와주세요. (https://gitlab.com/kuinsoo/linux/tree/master/Docker/windows_path)
```
docker run <옵션> <이미지 이름, ID> <명령> <매개 변수>
$ docker run -it -p 접속포트:80 -v /d/프로젝트폴더경로:/var/www/html/ --name 컨테이너이름  ubuntu:16.04 bash
root@d79d46ebe815:/# << 컨테이너에 접속 된 모습
```
> 컨테이너가 잘 올라가 있는지 확인 \
docker ps 로 실행 중인 컨테이너만 확인이 가능하며  docker ps -a 옵션을 붙이면 멈춰있는 컨테이너까지 전부 보여준다.
```
$ docker ps
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS                  NAMES
d79d46ebe815        ubuntu:16.04        "bash"              5 minutes ago       Up 5 minutes        0.0.0.0:8889->80/tcp   Forexliga
```
> 컨테이너 접속하기 \
docker exec -it 컨테이너이름 bash
```
$ docker exec -it Forexliga bash
root@d79d46ebe815:/#
```

## 설치 시작
> 업데이트 부터 해주겠습니다. \
컨테이는 기본적으로 root 접속이며 sudo 명령이 되지 않습니다.
```
$ apt -y update
```
> 아파치2 를 설치 하겠습니다  (버전은 최적화 버전으로 자동)
```
$ apt -y install apache2
```
> PHP7.0 버전이 명시되었기 때문에 버전을 지정하여 설치 하겠습니다.\
아파치와 연동 되어야하기때문에 설치 명렁어가 조금 다릅니다.
```
$ apt -y install libapache2-mod-php7.0
```
> PHP7.0 Extention 설치하기 ( 자신이 필요한 것만 따로 설치하셔도 됩니다. )
```
$ apt-get -y install php7.0-mysql php7.0-curl php7.0-gd php7.0-intl php-pear php-imagick php7.0-imap php7.0-mcrypt php-memcache  php7.0-pspell php7.0-recode php7.0-sqlite3 php7.0-tidy php7.0-xmlrpc php7.0-xsl php7.0-mbstring php-gettext
```

## 서버실행
> 서버 실행전에 일단 확인 진행. ( 아파치 재시작을 하면 도메인 이름을 설정해달라고 메시지가 올라온다. (무시))
```
$ service apache2 restart 
```
## 접속
1. kitematic 실행
2. 왼쪽에 컨테이너 이름 클릭
3. 우측 상단에 setting 클릭
4. hostname/port 탭 클릭 
5. ip 와 포트 확인
> http://ip주소:포트 접속

## php index 적용
> vim 에디터를 좋아하기 때문에 설치하겠습니다.
```
$ apt-get install -y vim
$ vi /var/www/html/index.php
```
> a or i 를 누른 후  아래와 같이 입력
```
<?php 
phpinfo();
?>
```
> esc 를 누른 후  : 입력 > wq 입력 > 엔터

> 기존 index.html 삭제 해준다. \
나는 이것때문에  index.php가 실행이 되지 않았다.
```
$ rm -r /var/www/html/index.html
```
> 브라우저를 새로고침 해준다.
