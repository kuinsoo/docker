# Docker image 만들기
도커 컨테이너를 이미지로 만들어 본다.\
docker commit <옵션> <컨테이너 이름> <dockerhub 아이디>/<이미지 이름>:<태그> \
-a : 이미지를 작성한 작성자 이름을 지정함 \
-m : 이미지 생성과 관련된 로그 메세지를 지정함
```sh
$ docker commit -a "구인수 kkuinsoo@gmail.com" -m "Mariadb 외부 서버용" mariadb kkuinsoo/mariadb:[tag명]
sha256:531bb193b1c6bc14c88037f79415be939aa4ec60bdd9a1f4fcd8ebb46c27e48d
PS C:\Windows\system32> docker images
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
mariadb             kku                 531bb193b1c6        5 seconds ago       438MB
```
> 이 옵션은 필수 조건이 아닙니다. 

# Docker hub 등록
만들어진 이미지를 도커 허브에 등록 시켜 보겠습니다. 

### 1. 로그인 진행 
```
$ docker login
name : 도커허브 아이디
password: 도커허브 비밀번호
Login Succeeded
```
### 2. push 하기 
도커 허브로 등록 시킨다.  \
docker push <커밋했던 이미지 이름> \
! 주의해야할 것은 꼭 이미지 커밋시  [허브아이디 / 이미지이름] 이어야 한다.
```
$ docker push kkuinsoo/mariadb
```
