# KKu Island <small>(꾸왈!)</small>
Mariadb 하나만 돌아가는 도커 컨테이너 생성 \
DB서버에 따로 돌려 놓으면 좋을 것 같다.


[client]
default-character-set=utf8mb4

[mysql]
default-character-set=utf8mb4

[mysqldump]
default-character-set=utf8mb4

[mysqld]
collation-server = utf8mb4_unicode_ci
init-connect='SET NAMES utf8mb4'
character-set-server = utf8mb4