# KKu Island <small>(꾸왈!)</small>
새로운 세상은 항상 힘들다.. \
모르니 몸과 머리가 다공생하네 ㅠ.ㅠ. \
흐름은 이렇다 
1. 윈도우 스케줄러 bat 파일 실행 
2. bat 파일 ps1 파워셀 파일 실행
3. PowerShell 은 docker container 컨트롤
> 거꾸로 진행 되긴했지만 이렇게 생각하기 까지.. cron.. rc.d.. sh...   등등 별짓을 다했다.

# Docker Container 컨트롤 하기 
컨테이너에 접속하지 않고 외부에서 명령어로 서비스 재시작 및 제어가 가능하다.\
docker exec -it 컨테이터명 bash [컨테이너 안에서 실행될 명령어]\
```sh
# 재시작 귀찮아서.. 간단하게 컨테이너 아파치2를 재시작 시켜보겠습니다.
$ docker exec -it uap bash service apache2 restart
```

# PowerShell 파일 만들기 (ps1)
자신이 원하는 위치에 <font color="green">[이름].ps1</font> 을 하나 만들어 준다.\
Powershell 에서 실행될 명령어를 작성한다.
```sh
$ docker exec -it uap bash service apache2 restart
```

# ps1 을 실행 시킬 bat 파일 작성
ps1 파일을 윈도우 실행 파일이 아니기 때문에 더블클릭을 해도 실행이 되지 않는다.\
그래서 bat 파일을 통해 ps1 를 컨트롤 하겠다.\
옵션들이 가지고 있는 내용까지는 공부하지 않겠다. 
```bat
:@echo off  : << bat 파일 주석이다.
	Powershell.exe -noprofile -executionpolicy bypass -file "D:\Development\Project\Docker\ubuntu.ps1"
:pause
```
> 파워셀을 실행시킨다. 그리고 ps1 파일이 있는 위치를 -file "안에" 작성해준다. \
주석을 풀면 실행 결과를 볼 수 있다.

# bat 파일일 더블클릭도 귀찮다. ( 윈도우 작업 스케줄러 연동 )
![윈도우스케줄러](window_scheduler.png)
윈도우키 > 작업 스케줄러 를 검색하면 나온다.\

1. 새로운 작업 스케줄러 등록
2. 일반 설정 ( 스케줄러 이름 등록 )
3. 트리거 설정 ( 언제 스크립트를 실행할지 시점을 선택 )
4. 동작 설정 ( 새로만들기를 누른후 실행될 bat 파일을 지정 )
5. 테스트로 실행 버튼을 눌러 확인 완료

# TIP 파워쉘 관리자권한 우회
1. 파워쉘 관리자 권한으로 열기 \
관리자 권한으로 파워쉘 오픈\
Set-ExecutionPolicy RemoteSigned // 권한 On \
Set-ExecutionPolicy Restricted // 권한 Off \
\
CMD 창에서 \
<code>$ powershell -file test.ps1</code> 수행 테스트

2. 우회방법\
cmd 창에서 -command 옵션으로 한줄로 작성하면 실행권한을 우회할 수 있다.\
Get-Process\
Get-Content test.txt\
\
<code>$ powershell -command Get-Process; Get-Content test.txt</code>


# 포트사용중일 경우 ( CMD 관리자권한 )
###  1. 포트 및 PID 확인 (8000 포트 예)
	netstat -ano | find "LISTENING" | find "8000"
	(의미 : 네트워크에서 열린 포트중에 8000 이 포함된 것들을 찾는다)
### 2. PID를 가진 프로세스 확인
	tasklist /FI "PID eq [PID번호]"
	(의미 : PID 가 [PID번호]인 프로세스를 확인한다)
### 3. PID 를 가진 프로세스 죽이기
	taskkill /F /PID [PID번호]
	(의미 : PID 가 [PID번호]인 프로세스를 죽인다)
### 4. 해당 포트를 사용하는 프로세를 직접 확인하는 방법
	for /f "tokens=5" %p in (' netstat -ano ^| find "LISTENING" ^| find "8000" ') do tasklist /FI "PID eq %p"
### 5. 해당 포트를 사용하는 프로세스를 직접 죽이기
	for /f "tokens=5" %p in (' netstat -ano ^| find "LISTENING" ^| find "8000" ') do taskkill /F "PID eq %p"