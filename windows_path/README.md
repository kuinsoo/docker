# KKu Island <small>(꾸왈!)</small>
아 경로 설정때문에 지옥을 맛봤다.\
영어로 찾던게 오히려 독이 되었다. 한글로 좋은 정보를 가져올 수 있었다. 

# 설정
1. Run Docker Toolbox from shortcut Docker Quickstart Terminal
2. Shutdown boot2docker docker-machine stop
3. Open VirtualBox and goto Settings of default machine
4. At Shared Folders menu, click + icon, Select folder path to drive D: and set folder name is d, checked all options
5. Copy <code>`attach-drive-d.sh`</code> script to Docker Toolbox folder (C:\Program Files\Docker Toolbox)
6. Edit <code>start.sh</code> add command sh <code>attach-drive-d.sh</code> before finalize step
7. Run Docker Toolbox from shortcut Docker Quickstart Terminal again
8. Have fun!!!

<code>attach-drive-d.sh</code>
```
#!/bin/bash

if [ "${VM}" == "default" ]; then
  "${DOCKER_MACHINE}" ssh "${VM}" "sudo mkdir -p /d/"
  "${DOCKER_MACHINE}" ssh "${VM}" "sudo mount -t vboxsf -o defaults,uid=\$(id -u docker),gid=\$(id -g docker) d /d/"
fi
```
<code>start.sh</code>
```
#Add script before finalize step

sh attach-drive-d.sh

STEP="Finalize"
#...
```

### 한글설명 (순서는 위와 다를 수 있다.)
도커 머신을 종료 시킨다. ex) docker-machine stop \
도커가 설치된 폴더로 간다 \
C:\Program Files\Docker Toolbox 기본 설치 위치\
<code>attach-drive-d.sh</code> 파일을 하나 만들어 위와 같이 소스를 넣어준다. \
<code>start.sh</code> 파일을 열어 <strong>STEP="Finalize"</strong> 를 찾아 위에 sh <code>start.sh</code> 넣어준다.\
도커 머신을 실행 시킨다. ex) docker-machine start 

# 참고
<a hre="https://velog.io/@public_danuel/volume-path-in-docker">Docker Toolbox 에서 D 드라이브 볼륨 경로 지정하기</a>\
<a hre="https://gist.github.com/first087/2214c81114f190271d26c3e88da36104">Git gist 영문 설명</a>
