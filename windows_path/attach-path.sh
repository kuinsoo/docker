#!/bin/bash

if [ "${VM}" == "default" ]; then
  "${DOCKER_MACHINE}" ssh "${VM}" "sudo mkdir -p /d/"
  "${DOCKER_MACHINE}" ssh "${VM}" "sudo mount -t vboxsf -o defaults,uid=\$(id -u docker),gid=\$(id -g docker) d /d/"
fi